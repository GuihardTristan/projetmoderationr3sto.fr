package controleur;

import dao.daoAvis;
import dao.daoAvisM;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import metier.avis;
import metier.avisM;
import metier.restaurants;
import metier.users;
import vue.ModeratorJFrame;

public class CtrlLesAvis implements WindowListener, ActionListener {

    private ModeratorJFrame vue; // LA VUE
    private CtrlPrincipal ctrlPrincipal;

    public CtrlLesAvis(ModeratorJFrame vue, CtrlPrincipal ctrl) throws SQLException {
        this.vue = vue;
        this.vue.addWindowListener(this);
        this.ctrlPrincipal = ctrl;
        // le contrôleur écoute la vue
        // préparer l'état iniitial de la vue
        afficherLesAvis();
        initialiserSelectionComboBox();
        configurerEcouteurComboBox();
        configurerEcouteurJButtonMasque();
    }

    // contrôle de la vue
    /**
     * Remplir le composant JTable avec les adresses
     *
     * @param desAdresses liste des adresses à afficher
     */
    public final void afficherLesAvis() throws SQLException {
        List<avis> lesAvis = null;
        lesAvis = daoAvis.selectAllAvis();
        getVue().getModeleTableAvis().setRowCount(0);
        String[] titresColonnes = {"Resto", "idR", "Note", "idU", "Commentaire", "Date"};
        getVue().getModeleTableAvis().setColumnIdentifiers(titresColonnes);
        for (avis unAvis : lesAvis) {
            Object[] row = {
                unAvis.getRestaurant().getNom(),
                unAvis.getRestaurant().getId(),
                unAvis.getNote(),
                unAvis.getUtilisateur().getId(),
                unAvis.getCommentaire(),
                unAvis.getDate()

            };
            getVue().getModeleTableAvis().addRow(row);
        }
    }

    // ACCESSEURS et MUTATEURS
    public ModeratorJFrame getVue() {
        return vue;
    }

    public void setVue(ModeratorJFrame vue) {
        this.vue = vue;
    }

    private void initialiserSelectionComboBox() {
        JComboBox<String> comboBox = vue.getjComboBox1();
        // Définir l'élément sélectionné dans la JComboBox
        String[] elements = {"1 semaine", "2 semaine", "3 semaine", "1 mois"};
        comboBox.removeAllItems();
        for (String element : elements) {
            comboBox.addItem(element);
        }
    }

    private void configurerEcouteurComboBox() {
        JComboBox<String> comboBox = vue.getjComboBox1();
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Réagir à l'action de l'utilisateur sur la JComboBox
                String selectedValue = (String) comboBox.getSelectedItem();
                // Effectuer différentes actions en fonction de la valeur sélectionnée
                if (selectedValue.equals("1 semaine")) {
                    try {
                        selectionTemps(7);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (selectedValue.equals("2 semaine")) {
                    try {
                        selectionTemps(14);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (selectedValue.equals("3 semaine")) {
                    try {
                        selectionTemps(21);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (selectedValue.equals("1 mois")) {
                    try {
                        selectionTemps(30);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private void selectionTemps(int jours) throws SQLException {
        List<avis> lesAvis = null;
        lesAvis = daoAvis.getAllAvisByDates(jours);
        getVue().getModeleTableAvis().setRowCount(0);
        String[] titresColonnes = {"Resto", "idR", "Note", "idU", "Commentaire", "Date"};
        getVue().getModeleTableAvis().setColumnIdentifiers(titresColonnes);
        for (avis unAvis : lesAvis) {
            Object[] row = {
                unAvis.getRestaurant().getNom(),
                unAvis.getRestaurant().getId(),
                unAvis.getNote(),
                unAvis.getUtilisateur().getId(),
                unAvis.getCommentaire(),
                unAvis.getDate()

            };
            getVue().getModeleTableAvis().addRow(row);
        }
    }

    private void configurerEcouteurJButtonMasque() {
        JButton masquerButton = vue.getjButtonMasque();;
        masquerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    masquerAvis();
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void masquerAvis() throws SQLException {
        List<avis> lesAvis = null;
        lesAvis = daoAvis.selectAllAvis();
        int selectedRow = vue.getjTableAvis().getSelectedRow();
        if (selectedRow != -1) {
            String raison = JOptionPane.showInputDialog(vue, "Entrez la raison du masquage de l'avis :");
            if (raison != null && !raison.isEmpty()) {
                int modelRowIndex = vue.getjTableAvis().convertRowIndexToModel(selectedRow);
                avis avisSelectionnes = lesAvis.get(modelRowIndex);
                int note = avisSelectionnes.getNote();
                String commentaire = avisSelectionnes.getCommentaire();
                Date date = avisSelectionnes.getDate();

                try {
                    restaurants resto = avisSelectionnes.getRestaurant();
                    users utilisateur = avisSelectionnes.getUtilisateur();
                    avisM avisMasque = new avisM(resto, note, utilisateur, commentaire, date, raison);


                    daoAvisM.ajouterAvisM(avisMasque);

                    System.out.println("cc");

                    daoAvis.deleteAvis(avisSelectionnes);

                    afficherLesAvis(); // rafraîchir les avis affichés
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlLesAvis.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("Veuillez fournir une raison valide pour le masquage de l'avis.");
            }
        } else {
            System.out.println("Veuillez sélectionner un avis à masquer.");
        }
    }

    private void quitter() {
        ctrlPrincipal.quitterApplication();
    }

    // REACTIONS EVENEMENTIELLES
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        quitter();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
