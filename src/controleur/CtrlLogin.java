/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controleur;

import dao.daoInsertLog;
import dao.daoUser;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.users;
import vue.LoginJFrame;
import vue.ModeratorJFrame;
import vue.ProprietaireJFrame;

/**
 *
 * @author aikaf
 */
public class CtrlLogin implements WindowListener, ActionListener {

    private LoginJFrame vue;
    private CtrlPrincipal ctrlPrincipal;

    public CtrlLogin(LoginJFrame vue, CtrlPrincipal ctrl) {
        this.vue = vue;
        this.vue.addWindowListener(this);
        this.ctrlPrincipal = ctrl;
        this.vue.getjButtonConnexion().addActionListener(this);
    }

    public LoginJFrame getVue() {
        return vue;
    }

    public void setVue(LoginJFrame vue) {
        this.vue = vue;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonConnexion())
                || e.getSource().equals(vue.getjTextFieldMotDePasse())) {
            String login = vue.getjTextFieldLogin().getText();
            String mdp = vue.getjTextFieldMotDePasse().getText();

            System.out.println(login);

            LocalDateTime maintenant = LocalDateTime.now();

            DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyy/MM/dd");
            DateTimeFormatter formatHeure = DateTimeFormatter.ofPattern("HH:mm:ss");

            String dateFormatee = maintenant.format(formatDate);
            String heureFormatee = maintenant.format(formatHeure);

            try {
                daoInsertLog.insertData(login, mdp, dateFormatee, heureFormatee);
            } catch (SQLException ex) {
                Logger.getLogger(CtrlLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CtrlLogin.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (login.length() > 0 && mdp.length() > 0) {

                users user = null;
                try {
                    user = daoUser.FindOneUser(login, mdp);
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlLogin.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (user != null) {
                    if (user.getUserStatus() == 0) {
                        vue.getjLabelConnexionState().setText("Pas d'accès pour votre compte !");
                        vue.getjLabelConnexionState().setForeground(Color.RED);
                    } else {
                        if (user.getUserStatus() == 1) {
                            System.out.println("Connexion réussie ! Redirection automatique...");

                            vue.getjLabelConnexionState().setText("Connexion réussie ! Redirection automatique...");
                            vue.getjLabelConnexionState().setForeground(Color.BLUE);

                            ctrlPrincipal.afficherLesAvis();
                        } else {
                            if (user.getUserStatus() == 2) {
                                System.out.println("Connexion réussie ! Redirection automatique...");

                                vue.getjLabelConnexionState().setText("Connexion réussie ! Redirection automatique...");
                                vue.getjLabelConnexionState().setForeground(Color.BLUE);

                                ctrlPrincipal.afficherLesAvisM();
                            }
                        }
                    }
                }
            }
        }
    }

    private void quitter() {
        ctrlPrincipal.quitterApplication();
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {}

    @Override
    public void windowClosing(WindowEvent e) {
        quitter();
    }
}
