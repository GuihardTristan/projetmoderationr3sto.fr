/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controleur;

import dao.daoAvis;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.List;
import metier.avis;
import vue.ModeratorJFrame;
import vue.ProprietaireJFrame;

/**
 *
 * @author aikaf
 */
public class CtrlLesAvisM implements WindowListener, ActionListener {

    private ProprietaireJFrame vue; // LA VUE
    private CtrlPrincipal ctrlPrincipal;

    public CtrlLesAvisM(ProprietaireJFrame vue, CtrlPrincipal ctrl) throws SQLException {
        this.vue = vue;
        this.ctrlPrincipal = ctrl;
        this.vue.addWindowListener(this);
        // le contrôleur écoute la vue
        // préparer l'état iniitial de la vue
        afficherLesAvis();
    }

    public final void afficherLesAvis() throws SQLException {
        List<avis> lesAvis = null;
        lesAvis = daoAvis.selectAllAvis();
        getVue().getModeleTableAvis().setRowCount(0);
        String[] titresColonnes = {"Resto", "idR", "Note", "idU", "Commentaire", "Date"};
        getVue().getModeleTableAvis().setColumnIdentifiers(titresColonnes);
        for (avis unAvis : lesAvis) {
            Object[] row = {
                unAvis.getRestaurant().getNom(),
                unAvis.getRestaurant().getId(),
                unAvis.getNote(),
                unAvis.getUtilisateur().getId(),
                unAvis.getCommentaire(),
                unAvis.getDate()

            };
            getVue().getModeleTableAvis().addRow(row);
        }
    }

    public ProprietaireJFrame getVue() {
        return vue;
    }

    public void setVue(ProprietaireJFrame vue) {
        this.vue = vue;
    }

    private void quitter() {
        ctrlPrincipal.quitterApplication();
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        quitter();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

}
