/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controleur;

import javax.swing.JOptionPane;

/**
 *
 * @author aikaf
 */
public class CtrlPrincipal {

    private CtrlLesAvis ctrlLesAvis;
    private CtrlLogin ctrlLogin;
    private CtrlLesAvisM ctrlLesAvisM;

    public CtrlLesAvis getCtrlLesAvis() {
        return ctrlLesAvis;
    }

    public CtrlLogin getCtrlLogin() {
        return ctrlLogin;
    }

    public void setCtrlLesAvis(CtrlLesAvis ctrlLesAvis) {
        this.ctrlLesAvis = ctrlLesAvis;
    }

    public void setCtrlLogin(CtrlLogin ctrlLogin) {
        this.ctrlLogin = ctrlLogin;
    }

    public CtrlLesAvisM getCtrlLesAvisM() {
        return ctrlLesAvisM;
    }

    public void setCtrlLesAvisM(CtrlLesAvisM ctrlLesAvisM) {
        this.ctrlLesAvisM = ctrlLesAvisM;
    }

    public void afficherLesAvis() {
        this.ctrlLogin.getVue().setVisible(false);
        this.ctrlLesAvis.getVue().setVisible(true);
    }

    public void afficherLogin() {
        this.ctrlLesAvis.getVue().setVisible(false);
        this.ctrlLogin.getVue().setVisible(true);
    }

    public void afficherLesAvisM() {
        this.ctrlLogin.getVue().setVisible(false);
        this.ctrlLesAvisM.getVue().setVisible(true);
    }

    public void quitterApplication() {
        // Confirmer avant de quitter
        int rep = JOptionPane.showConfirmDialog(null, "Quitter l'application\nEtes-vous sûr(e) ? ", "  AGENCEB ", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rep == JOptionPane.YES_OPTION) {
            // mettre fin à l'application
            System.exit(0);
        }
    }

}
