/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import com.sun.jdi.connect.spi.Connection;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author ugosi
 */
public class daoInsertLog {

    public static void insertData(String pseudo, String password, String date, String heure) throws SQLException, IOException {
        PreparedStatement pstmt = null;
        Connection connexion = null;
        try {
            Jdbc jdbc = Jdbc.getInstance();
            String requete = "INSERT INTO logs (pseudoU, mdpU, date, heure) VALUES (?, ?, ?, ?)";
            pstmt = jdbc.getConnexion().prepareStatement(requete);
            pstmt.setString(1, pseudo);
            pstmt.setString(2, password);
            pstmt.setString(3, date);
            pstmt.setString(4, heure);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace(); // Imprimez les détails de l'exception pour le débogage
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (connexion != null) {
                    connexion.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
