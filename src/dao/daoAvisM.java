/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import metier.avisM;
import metier.restaurants;
import metier.users;

/**
 *
 * @author ugosi
 */
public class daoAvisM {

    /**
     *
     * @return @throws SQLException
     */
    public static List<avisM> selectAllAvisM() throws SQLException {
        List<avisM> lesAvis = new ArrayList<avisM>();
        avisM unAvisM = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM critiquer";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unAvisM = daoAvisM.AvisMFromResultSet(rs);
            lesAvis.add(unAvisM);
        }
        return lesAvis;
    }

    public static List<avisM> getAllAvisByDates(int temps) throws SQLException {
        List<avisM> lesAvisM = new ArrayList<avisM>();
        avisM unAvisM = null;
        ResultSet rs;
        LocalDate dateActuelle = LocalDate.now();
        LocalDate dateUtile = dateActuelle.minusDays(temps);

        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM critiquerm WHERE DATE_POST >= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setDate(1, java.sql.Date.valueOf(dateUtile));
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unAvisM = daoAvisM.AvisMFromResultSet(rs);
            lesAvisM.add(unAvisM);
        }
        return lesAvisM;
    }

    
    public static void ajouterAvisM(avisM avisMasque) throws SQLException {
        Connection cnx = null;

        PreparedStatement pstmt = null;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
            cnx = jdbc.getConnexion();
            String requete = "INSERT INTO critiquerm (idR, note, commentaire, idU, DATE_POST, raison) VALUES (?, ?, ?, ?, ?, ?);";
            pstmt = cnx.prepareStatement(requete);
            pstmt.setInt(1, avisMasque.getRestaurant().getId());
            pstmt.setInt(2, avisMasque.getNote());
            pstmt.setString(3, avisMasque.getCommentaire());
            pstmt.setInt(4, avisMasque.getUtilisateur().getId());
            pstmt.setDate(5, (Date) avisMasque.getDate());
            pstmt.setString(6, avisMasque.getRaison());
            pstmt.executeUpdate();
  
    }

    private static avisM AvisMFromResultSet(ResultSet rs) throws SQLException {
        avisM avsM = null;
        int idRestaurants = rs.getInt("idR");
        int note = rs.getInt("note");
        int idU = rs.getInt("idU");
        String commentaire = rs.getString("commentaire");
        Date date = rs.getDate("DATE_POST");
        String raison = rs.getString("raison");
        restaurants unRestaurant = daoResto.selectOneResto(idRestaurants);
        users unUser = daoUser.selectOneUser(idU);
        avsM = new avisM(unRestaurant, note, unUser, commentaire, date, raison);
        return avsM;
    }
}
