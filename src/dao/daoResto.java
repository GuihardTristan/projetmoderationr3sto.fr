/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import metier.avis;
import metier.restaurants;
import metier.users;

/**
 *
 * @author ugosi
 */
public class daoResto {

    public static List<restaurants> selectAllResto() throws SQLException {
        List<restaurants> lesRestaurants = new ArrayList<restaurants>();
        restaurants unRestaurant = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM CLIENT";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unRestaurant = daoResto.RestoFromResultSet(rs);
            lesRestaurants.add(unRestaurant);
        }
        return lesRestaurants;
    }
    public static restaurants selectOneResto(int idR) throws SQLException {
        restaurants unRestaurant = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM resto WHERE idR= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idR);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            unRestaurant = daoResto.RestoFromResultSet(rs);
        }
        return unRestaurant;
    }
    private static restaurants RestoFromResultSet(ResultSet rs) throws SQLException {
        restaurants resto = null;
        int idRestaurants = rs.getInt("idR");
        String nomResto = rs.getString("nomR");
        int numAdresse = rs.getInt("numAdrR");
        String voieAdr = rs.getString("voieAdrR");
        String codePostResto = rs.getString("cpR");
        String villeResto = rs.getString("villeR");
        String descriptionResto = rs.getString("descR");
        resto = new restaurants(idRestaurants,nomResto,numAdresse,voieAdr,codePostResto,villeResto,descriptionResto);
        return resto;
    }
}
