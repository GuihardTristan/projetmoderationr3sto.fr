/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import at.favre.lib.crypto.bcrypt.BCrypt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import metier.users;

/**
 *
 * @author ugosi
 */
public class daoUser {

    public static users selectOneUser(int idU) throws SQLException {
        users unUser = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM utilisateur WHERE idU= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idU);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            unUser = daoUser.RestoFromResultSet(rs);
        }
        return unUser;
    }

    public static List<users> selectAll() throws SQLException {
        List<users> lesUsers = new ArrayList<users>();
        users unUser;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM utilisateur";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unUser = daoUser.RestoFromResultSet(rs);
            lesUsers.add(unUser);
        }
        return lesUsers;
    }

    public static users FindOneUser(String Pseudo, String Mdp) throws SQLException {
        users unUser;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM utilisateur WHERE pseudoU= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, Pseudo);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            String hashedPassword = rs.getString("mdpU");

            // Vérifier si le mot de passe correspond au mot de passe stocké dans la bdd
            boolean isValid = BCrypt.verifyer().verify(Mdp.toCharArray(), hashedPassword).verified;

            if (isValid) {
                // Création d'un objet User avec les informations récupérées de la base de données
                unUser = daoUser.RestoFromResultSet(rs);

                return unUser;
            }
        }
        return null;
    }

    private static users RestoFromResultSet(ResultSet rs) throws SQLException {
        int idUtilisateurs = rs.getInt("idU");
        String mailUtilisateur = rs.getString("mailU");
        String mdpUtilisateur = rs.getString("mdpU");
        String pseudoUtilisateur = rs.getString("pseudoU");
        int StatusUtilisateur = rs.getInt("userStatus");
        int restoUsers = rs.getInt("restoUser");
        users utilisateur = new users(idUtilisateurs, mailUtilisateur, mdpUtilisateur, pseudoUtilisateur, StatusUtilisateur, restoUsers);
        return utilisateur;
    }
}
