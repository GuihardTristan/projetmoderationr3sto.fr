/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import metier.avis;
import metier.avisM;
import metier.restaurants;
import metier.users;

/**
 *
 * @author ugosi
 */
public class daoAvis {

    /**
     *
     * @return @throws SQLException
     */
    public static List<avis> selectAllAvis() throws SQLException {
        List<avis> lesAvis = new ArrayList<avis>();
        avis unAvis = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM critiquer";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unAvis = daoAvis.AvisFromResultSet(rs);
            lesAvis.add(unAvis);
        }
        return lesAvis;
    }

    public static List<avis> getAllAvisByDates(int temps) throws SQLException {
        List<avis> lesAvis = new ArrayList<avis>();
        avis unAvis = null;
        ResultSet rs;
        LocalDate dateActuelle = LocalDate.now();
        LocalDate dateUtile = dateActuelle.minusDays(temps);

        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM critiquer WHERE DATE_POST >= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setDate(1, java.sql.Date.valueOf(dateUtile));
        rs = pstmt.executeQuery();
        while (rs.next()) {
            unAvis = daoAvis.AvisFromResultSet(rs);
            lesAvis.add(unAvis);
        }
        return lesAvis;
    }

    public static void deleteAvis(avis avisMasque) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt;
        System.out.println("Je suis lancée");
        String requete = "DELETE FROM critiquer WHERE idR = ? AND note = ? AND commentaire = ? AND idU = ? AND DATE_POST = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, avisMasque.getRestaurant().getId());
        pstmt.setInt(2, avisMasque.getNote());
        pstmt.setString(3, avisMasque.getCommentaire());
        pstmt.setInt(4, avisMasque.getUtilisateur().getId());
        pstmt.setDate(5, (Date) avisMasque.getDate());
        pstmt.executeUpdate();
    }

    private static avis AvisFromResultSet(ResultSet rs) throws SQLException {
        avis avs = null;
        int idRestaurants = rs.getInt("idR");
        int note = rs.getInt("note");
        int idU = rs.getInt("idU");
        String commentaire = rs.getString("commentaire");
        Date date = rs.getDate("DATE_POST");
        restaurants unRestaurant = daoResto.selectOneResto(idRestaurants);
        users unUser = daoUser.selectOneUser(idU);
        avs = new avis(unRestaurant, note, unUser, commentaire, date);
        return avs;
    }
}
