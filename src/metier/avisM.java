/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package metier;

import java.util.Date;

/**
 *
 * @author ugosi
 */
public class avisM {

    public avisM(restaurants restaurant, int Note, users utilisateur, String commentaire, Date date, String raison) {
        this.restaurant = restaurant;
        this.Note = Note;
        this.utilisateur = utilisateur;
        this.commentaire = commentaire;
        this.date = date;
        this.raison = raison;
    }

    //Attributs 
    private restaurants restaurant;
    private int Note;
    private users utilisateur;
    private String commentaire;
    private Date date;
    private String raison;

    public restaurants getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(restaurants restaurant) {
        this.restaurant = restaurant;
    }

    public int getNote() {
        return Note;
    }

    public void setNote(int Note) {
        this.Note = Note;
    }

    public users getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(users utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRaison() {
        return raison;
    }

    public void setRaison(String raison) {
        this.raison = raison;
    }

}
