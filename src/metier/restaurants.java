/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package metier;

/**
 *
 * @author ugosi
 */
public class restaurants {

    public restaurants(int id, String nom, int numAdresse, String adresse, String CP, String ville, String desc) {
        this.id = id;
        this.nom = nom;
        this.numAdresse = numAdresse;
        this.adresse = adresse;
        this.CP = CP;
        this.ville = ville;
        this.desc = desc;
    }
    //Ajout d'u champ dans la base de données
    //ALTER TABLE utilisateur ADD COLUMN restoUser bigint, ADD CONSTRAINT fk_resto_id FOREIGN KEY (restoUser) REFERENCES resto(idR);
    //ALTER TABLE utilisateur ADD COLUMN userStatus;
    // Attributs de la classe Restaurant
    private int id;
    private String nom;
    private int numAdresse;
    private String adresse;
    private String CP;
    private String ville;
    private String desc;

    //Getters et setters auto généréss
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumAdresse() {
        return numAdresse;
    }

    public void setNumAdresse(int numAdresse) {
        this.numAdresse = numAdresse;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
