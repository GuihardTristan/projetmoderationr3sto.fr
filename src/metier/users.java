package metier;

public class users {

    @Override
    public String toString() {
        return "users{" + "id=" + id + ", pseudo=" + pseudo + ", password=" + password + ", mail=" + mail + ", userStatus=" + userStatus + ", restoUser=" + restoUser + '}';
    }

    public users(int id, String pseudo, String password, String mail, int userStatus, int restoUser) {
        this.id = id;
        this.pseudo = pseudo;
        this.password = password;
        this.mail = mail;
        this.userStatus = userStatus;
        this.restoUser = restoUser;
    }

    // Attributs de la classe
    private int id;
    private String pseudo;
    private String password;
    private String mail;
    private int userStatus;
    private int restoUser;

    // Constructeur
    

    // Getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRestoUser() {
        return restoUser;
    }

    public void setRestoUser(int restoUser) {
        this.restoUser = restoUser;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    // Autres méthodes si nécessaire
}
