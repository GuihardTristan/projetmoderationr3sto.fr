/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lanceur;

import com.sun.tools.javac.Main;
import controleur.CtrlLesAvis;
import controleur.CtrlLesAvisM;
import controleur.CtrlLogin;
import controleur.CtrlPrincipal;
import dao.Jdbc;
import java.lang.System.Logger;
import java.sql.SQLException;
import vue.*;
import vue.ModeratorJFrame;

/**
 *
 * @author aikaf
 */
public class main {

    public static void main(String[] args) throws ClassNotFoundException {
        // Initialisation de la connexion à la base de données
        Jdbc.creer("jdbc:mysql://10.15.253.250/", "tguihard_bdd_resto?zeroDateTimeBehavior=convertToNull&serverTimezone=Europe/Paris", "tguihard", "dnJ=5RU:2w");
        try {
            // Connexion à la base de données
            Jdbc.getInstance().connecter();
            
            // Création des instances de vues
            LoginJFrame laVueLogin = new LoginJFrame();
            ModeratorJFrame laVueModerator = new ModeratorJFrame();
            ProprietaireJFrame laVueProprietaire = new ProprietaireJFrame();
            
            // Création des instances de contrôleur
            CtrlPrincipal leControleurPrincipal = new CtrlPrincipal();
            CtrlLogin leControleurLogin = new CtrlLogin(laVueLogin, leControleurPrincipal);
            CtrlLesAvis leControleurLesAvis = new CtrlLesAvis(laVueModerator, leControleurPrincipal);
            CtrlLesAvisM LeControleurLesAvisM = new CtrlLesAvisM(laVueProprietaire,leControleurPrincipal);
            
            // Attribution des instances de contrôleur à CtrlPrincipal
            leControleurPrincipal.setCtrlLogin(leControleurLogin);
            leControleurPrincipal.setCtrlLesAvis(leControleurLesAvis);
            leControleurPrincipal.setCtrlLesAvisM(LeControleurLesAvisM);
            
            // Affichage de la vue de connexion
            laVueLogin.setVisible(true);

        } catch (SQLException ex) {
            // Gestion des exceptions liées à la connexion à la base de données
            System.out.println(ex.getMessage());
        }
    }
}
